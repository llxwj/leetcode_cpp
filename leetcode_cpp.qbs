import qbs

CppApplication {
    type: "application" // To suppress bundle generation on Mac
    consoleApplication: true
    files: [
        "main.cpp",
    ]
    cpp.includePaths:["./",]
    cpp.cppFlags:["-std=c++11"]
    cpp.staticLibraries:["gtest", "stdc++"]

    Group {     // Properties for the produced executable
        fileTagsFilter: product.type
        qbs.install: true
    }
}

