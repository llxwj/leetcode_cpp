TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += leetcode.cpp

LIBS += -L$$PWD
LIBS += -lgtest
LIBS += -lstdc++

QMAKE_CXXFLAGS +=-std=c++11

include(deployment.pri)
qtcAddDeployment()

